#ifndef VISUALTABLE_H
#define VISUALTABLE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "Table.h"
#include "Field.h"


class VisualTable : public Table {
public:
    VisualTable(Field* field);
    virtual ~VisualTable();

    virtual void show();
    virtual void update();

    void handleEvents();
    void handleMouseClick(int button, int x, int y);
    void getMouseTarget(int x, int y, int& row, int& col, int& digit);

    void pause(int ms);

    bool active;
protected:
private:
    void renderTable();

    void renderSymbol(char c, SDL_Rect dest, int sz);
    SDL_Rect getPos(int row, int col);
    SDL_Rect getCandidatePos(int row, int col, int digit);
    void renderDigit(int row, int col, int digit);
    void renderCandidate(int row, int col, int digit);
    void drawMark(int row, int col, int type);

    Field* field;
    int step;
    int width;

    SDL_Renderer *renderer;
    SDL_Window *window;

    SDL_Texture* digits[9];

    TTF_Font* font[2];

    int marked;
};

#endif // VISUALTABLE_H
