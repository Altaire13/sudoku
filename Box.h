#ifndef BOX_H_INCLUDED
#define BOX_H_INCLUDED

extern char b[38][4];

enum B {
    LH,
    HH,
    LV,
    HV,

    HDR,
    HDL,
    HUR,
    HUL,

    LDR,
    LDL,
    LUR,
    LUL,

    VHRL,
    HVR,
    VHLL,
    HVL,

    DLHH,
    HDH,
    ULHH,
    HUH,

    LVR,
    LVL,
    LDH,
    LUH,

    LVH,
    VLHH,
    VHHL,
    HVH,

    D0, D1, D2, D3, D4, D5, D6, D7, D8, D9
};


#endif // BOX_H_INCLUDED
