#include "VisualTable.h"

VisualTable::VisualTable(Field* field) : active(true), field(field), step(90), width(11 * step), marked(0) {
    SDL_Init(SDL_INIT_VIDEO);
    std::cout << TTF_Init() << std::endl;

    std::cout << "After RW: " << SDL_GetError() << std::endl;

    //int width = 11 * step;

    window = SDL_CreateWindow("Visual table",
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              width, width,
                              0);
    std::cout << "After window: " << SDL_GetError() << std::endl;
    renderer = SDL_CreateRenderer(window, -1, 0);

    std::cout << "After renderer: " << SDL_GetError() << std::endl;

    renderTable();

    font[0] = TTF_OpenFont("DejaVuSans.ttf", step);
    if (font[0] == nullptr) {
        std::cout << "Error loading font:\n";
        std::cout << TTF_GetError() << std::endl;
    }

    font[1] = TTF_OpenFont("DejaVuSans.ttf", step / 3);
    if (font[1] == nullptr) {
        std::cout << "Error loading font:\n";
        std::cout << TTF_GetError() << std::endl;
    }

//    renderSymbol('1', step, step, 0);
  //  renderSymbol('1', 2 * step, step, 1);
    /*renderDigit(0, 0, 3);
    renderCandidate(1, 1, 3);

    for (int i = 1; i <= 9; ++i) {
        renderCandidate(2, 2, i);
    }*/
}

VisualTable::~VisualTable() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

void VisualTable::show() {
    SDL_RenderPresent(renderer);
}

void VisualTable::update() {
    //SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_SetRenderDrawColor(renderer, 0xCC, 0x77, 0x22, 0xFF);
    SDL_RenderClear(renderer);

    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (field->cells[i][j]) {
                if (field->cells[i][j] == marked) {
                    drawMark(i, j, 0);
                }
                renderDigit(i, j, field->cells[i][j]);
            } else {
                if (marked && field->candidates[i][j][marked-1]) {
                    drawMark(i, j, 1);
                }
                for (int k = 0; k < 9; ++k) {
                    if (field->candidates[i][j][k]) {
                        renderCandidate(i, j, k + 1);
                    }
                }
            }
        }
    }

    renderTable();
}

void VisualTable::handleEvents() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            active = false;
            return;
        } else if (event.type == SDL_KEYDOWN &&
                   event.key.keysym.sym == SDLK_ESCAPE) {
            active = false;
            return;
        } else if (event.type == SDL_MOUSEBUTTONDOWN) {
            SDL_MouseButtonEvent b = event.button;
            handleMouseClick(b.button, b.x, b.y);
        }
    }
}

void VisualTable::handleMouseClick(int button, int x, int y) {
    int row, col, digit;
    getMouseTarget(x, y, row, col, digit);
    if (button == 1 && field->cells[row][col] == 0) {
        field->candidates[row][col][digit] ^= true;
    } else if (button == 3) {
        marked = digit + 1;
    }
}

void VisualTable::getMouseTarget(int x, int y, int& row, int& col, int& digit) {
    row = (y - step) / step;
    col = (x - step) / step;

    x -= (col + 1) * step;
    y -= (row + 1) * step;

    int c = x / (step / 3);
    int r = y / (step / 3);

    digit = 3 * r + c;
}

void VisualTable::pause(int ms) {
    SDL_Delay(ms);
}

void VisualTable::renderTable() {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    for (int i = 0; i < 10; ++i) {
        SDL_RenderDrawLine(renderer, step + i * step, step, step + i * step, width - step);
        SDL_RenderDrawLine(renderer, step, step + i * step, width - step, step + i * step);
    }

    for (int i = 0; i < 4; ++i) {
        SDL_RenderDrawLine(renderer, step + 1 + i * step * 3, step, step + 1 + i * step * 3, width - step);
        SDL_RenderDrawLine(renderer, step, step + 1 + i * step * 3, width - step, step + 1 + i * step * 3);
    }
}

void VisualTable::renderSymbol(char c, SDL_Rect dest, int sz) {
    char s[2] = {c, 0};
    SDL_Color white = {255, 255, 255, 255};
    SDL_Color black = {0, 0, 0, 0};
    SDL_Color chooser[2] = {black, white};
    SDL_Surface* surf = TTF_RenderText_Blended(font[sz], s, chooser[sz]);

    int offset_x = 0;
    int offset_y = 0;
    if (sz == 0) {
        offset_x = (step - surf->w) / 2;
        offset_y = (step - surf->h) / 2;
    } else {
        offset_x = (step / 3 - surf->w) / 2;
        offset_y = (step / 3 - surf->h) / 2;
    }

    dest.x += offset_x;
    dest.y += offset_y;

    dest.w = surf->w;
    dest.h = surf->h;
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surf);
    SDL_RenderCopy(renderer, texture, NULL, &dest);
    SDL_FreeSurface(surf);
    SDL_DestroyTexture(texture);
    //SDL_RenderDrawRect(renderer, &dest);
}

SDL_Rect VisualTable::getPos(int row, int col) {
    SDL_Rect rect;
    rect.x = (col + 1) * step;
    rect.y = (row + 1) * step;
    return rect;
}

SDL_Rect VisualTable::getCandidatePos(int row, int col, int digit) {
    digit--;
    SDL_Rect rect = getPos(row, col);
    rect.x += (step / 3) * (digit % 3);
    rect.y += (step / 3) * (digit / 3);
    return rect;
}

void VisualTable::renderDigit(int row, int col, int digit) {
    SDL_Rect dest = getPos(row, col);
    renderSymbol('0' + digit, dest, 0);
}

void VisualTable::renderCandidate(int row, int col, int digit) {
    SDL_Rect dest = getCandidatePos(row, col, digit);
    renderSymbol('0' + digit, dest, 1);
}

void VisualTable::drawMark(int row, int col, int type) {
    SDL_Rect dest = getPos(row, col);
    dest.w = step;
    dest.h = step;

    SDL_SetRenderDrawColor(renderer, 100 + 100 * type, 100 + 100 * type, 255, 0);
    SDL_RenderFillRect(renderer, &dest);
}
