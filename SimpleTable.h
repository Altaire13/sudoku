#ifndef SIMPLETABLE_H
#define SIMPLETABLE_H

#include "Table.h"
#include "Field.h"


class SimpleTable : public Table {
public:
    SimpleTable(Field* field);
    virtual ~SimpleTable();

    virtual void show();
    virtual void update();
protected:
private:
    Field* field;

    const char* view[13][14];
};

#endif // SIMPLETABLE_H
