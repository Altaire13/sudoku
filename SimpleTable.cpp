#include "SimpleTable.h"

SimpleTable::SimpleTable(Field* field) : field(field) {
    for (int i = 0; i < 13; ++i) {
        view[i][13] = "\n";
        for (int j = 0; j < 13; ++j) {
            view[i][j] = "*";
        }
    }

    for (int i = 0; i < 4; ++i) {
        for (int j = 1; j < 12; ++j) {
            view[4*i][j] = b[LH];
            view[j][4*i] = b[LV];
        }
    }

    for (int i = 1; i < 3; ++i) {
        for (int j = 1; j < 3; ++j) {
            view[4*i][4*j] = b[LVH];
        }
    }

    for (int i = 1; i < 3; ++i) {
        view[4*i][0] = b[LVR];
        view[4*i][12] = b[LVL];
        view[0][4*i] = b[LDH];
        view[12][4*i] = b[LUH];
    }

    view[0][0] = b[LDR];
    view[0][12] = b[LDL];
    view[12][0] = b[LUR];
    view[12][12] = b[LUL];
}

SimpleTable::~SimpleTable() {
}

void SimpleTable::show() {
    for (int i = 0; i < 13; ++i) {
        for (int j = 0; j < 14; ++j) {
            cout << view[i][j];
        }
    }
}

void SimpleTable::update() {
    for (int i = 0; i < 3; ++i) {
        for (int ii = 0; ii < 3; ++ii) {
            for (int j = 0; j < 3; ++j) {
                for (int jj = 0; jj < 3; ++jj) {
                    int t;
                    if ((t = field->cells[3*i+ii][3*j+jj]) != 0) {
                        view[4*i+ii+1][4*j+jj+1] = b[D0+t];
                    }
                    else {
                        view[4*i+ii+1][4*j+jj+1] = " ";
                    }
                }
            }
        }
    }
}
