#include "BigTable.h"

BigTable::BigTable(Field* field) : field(field) {
    putNet(0, 0, 1, 1, 37, 37, "0");
    putNet(0, 37, 1, 1, 37, 1, "\n");

    putNet(4, 1, 4, 1, 8, 35, b[LH]); //Hor and vert light
    putNet(1, 4, 1, 4, 35, 8, b[LV]);

    putNet(0, 1, 12, 1, 4, 35, b[HH]); //Hor and vert heavy
    putNet(1, 0, 1, 12, 35, 4, b[HV]);

    putNet(4, 4, 4, 4, 8, 8, b[LVH]); //Light crosses

    putNet(4, 12, 4, 12, 8, 2, b[VHHL]); //Inner intercept
    putNet(12, 4, 12, 4, 2, 8, b[VLHH]);

    putNet(12, 12, 12, 12, 2, 2, b[HVH]); //Heavy crosses

    for (int i = 1; i < 9; ++i) {
        view[4*i][0] = b[VHRL];
        view[4*i][36] = b[VHLL];
        view[0][4*i] = b[DLHH];
        view[36][4*i] = b[ULHH];
    }

    for (int i = 1; i < 3; ++i) {
        view[12*i][0] = b[HVR];
        view[12*i][36] = b[HVL];
        view[0][12*i] = b[HDH];
        view[36][12*i] = b[HUH];
    }

    view[0][0] = b[HDR];
    view[0][36] = b[HDL];
    view[36][0] = b[HUR];
    view[36][36] = b[HUL];
}

BigTable::~BigTable() {
    //dtor
}

void BigTable::show() {
    for (int i = 0; i < 37; ++i) {
        for (int j =0; j < 38; ++j) {
            cout << view[i][j];
        }
    }
}

void BigTable::update() {
    for (int i = 0; i < 3; ++i) {
        for (int ii = 0; ii < 3; ++ii) {
            for (int j = 0; j < 3; ++j) {
                for (int jj = 0; jj < 3; ++jj) {
                    if (field->cells[3*i+ii][3*j+jj]) {
                        for (int k = 0; k < 9; ++k) {
                            view[12*i+4*ii+(k/3)+1][12*j+4*jj+(k%3)+1] = ".";
                        }
                        view[12*i+4*ii+2][12*j+4*jj+2] = b[D0+field->cells[3*i+ii][3*j+jj]];
                    } else {
                        for (int k = 0; k < 9; ++k) {
                            const char* d = " ";
                            if (field->candidates[3*i+ii][3*j+jj][k]) {
                                d = b[D1+k];
                            }
                            view[12*i+4*ii+(k/3)+1][12*j+4*jj+(k%3)+1] = d;
                        }
                    }
                }
            }
        }
    }
}

void BigTable::putNet(int x0, int y0, int dx, int dy, int nx, int ny, const char* s) {
    for (int i = 0; i < nx; ++i) {
        for (int j = 0; j < ny; ++j) {
            view[x0+i*dx][y0+j*dy] = s;
        }
    }
}
