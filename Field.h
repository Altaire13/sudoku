#ifndef FIELD_H
#define FIELD_H

#include <vector>
#include <cstdio>
#include <iostream>

#include "Box.h"
#include "Table.h"

#define N 9

using namespace std;

class BigTable;
class SimpleTable;
class VisualTable;

class Field {
public:
    Field();
    Field(Field& other);
    virtual ~Field();

    void copyField(Field& other);

    int putDigit(int row, int col, int digit);
    int putDigitUpdate(int row, int col, int digit);

    int fileInput(FILE* input);
    int fileOutput(FILE* output);

    int fullCandidateUpdateSimple();

    bool full();

    int openSingles();
    int closedSingles();

    void print(int t);

    int beginnerTry();

    int recursiveSolve(int maxDepth);

    int minDepth(int maxDepth);

    void addTable(Table *);
protected:
private:
    uint8_t cells[9][9];
    bool candidates[9][9][9];

    vector<Table*> tables;

    friend BigTable;
    friend SimpleTable;
    friend VisualTable;
};

#endif // FIELD_H
