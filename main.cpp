#include <iostream>
#include <istream>
#include <cstdio>

#include <iomanip>
#include <sstream>

#include "Box.h"
#include "Field.h"
#include "Table.h"
#include "BigTable.h"
#include "SimpleTable.h"
#include "VisualTable.h"

using namespace std;

int main(int argc, char** argv) {
    Field field;

    FILE* input = fopen("Tests/e7.txt", "r");
    FILE* output = fopen("Tests/expertResult.txt", "w");
    if (field.fileInput(input)) {
        std::cout << "File input error\n";
        return 0;
    }

    VisualTable vTable(&field);

    BigTable bigTable(&field);
    field.addTable(&bigTable);

    std::cout << "Task:\n";
    bigTable.update();
    bigTable.show();

    SimpleTable sTable(&field);
    field.addTable(&sTable);
    sTable.update();
    sTable.show();

    int status, maxDepth = 2;
/*
    std::cout << "Solving\n";
    //std::cout << field.beginnerTry() << std::endl;
    //status = field.recursiveSolve(2);
    //std::cout << "Status: " << status << std::endl;
    status = field.minDepth(maxDepth);
    if (status >= 0) {
        std::cout << "Min depth: " << status << std::endl;
    } else if (status == -1) {
        std::cout << "Can't solve (max depth was: " << maxDepth << ").\n";
    } else if (status == -2) {
        std::cout << "Wrong task!\n" << std::endl;
    }*/

    std::cout << "Result:\n";
    bigTable.update();
    bigTable.show();
    sTable.update();
    sTable.show();

    field.fileOutput(output);

    status = field.beginnerTry();

    vTable.update();
    while(vTable.active) {
        vTable.handleEvents();
        if (status != -1) {
            status = field.beginnerTry();
        }
        vTable.update();
        vTable.show();
        vTable.pause(16);
    }

    while (vTable.active) {
        vTable.pause(16);
    }


    return 0;
}
