#ifndef BIGTABLE_H
#define BIGTABLE_H

#include "Table.h"
#include "Field.h"
#include "Box.h"

using namespace std;

class BigTable : public Table {
public:
    BigTable(Field* field);
    virtual ~BigTable();

    virtual void show();
    virtual void update();
protected:
private:
    Field* field;

    const char* view[37][38];

    void putNet(int x0, int y0, int dx, int dy, int nx, int ny, const char* s);
};

#endif // BIGTABLE_H
