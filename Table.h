#ifndef TABLE_H
#define TABLE_H


class Table {
public:
    Table();
    virtual ~Table();

    virtual void show() = 0;
    virtual void update() = 0;
protected:
private:
};

#endif // TABLE_H
