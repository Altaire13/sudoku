#include "Field.h"

Field::Field() {
    for (int row = 0 ; row < 9; ++row) {
        for (int col = 0; col < 9; ++col) {
            cells[row][col] = 0;

            for (int k = 0; k < 9; ++k) {
                candidates[row][col][k] = true;
            }
        }
    }
}

Field::Field(Field& other) {
    for (int row = 0 ; row < 9; ++row) {
        for (int col = 0; col < 9; ++col) {
            cells[row][col] = other.cells[row][col];

            for (int k = 0; k < 9; ++k) {
                candidates[row][col][k] = other.candidates[row][col][k];
            }
        }
    }
}

Field::~Field() {
    //dtor
}

void Field::copyField(Field& other) {
    for (int row = 0 ; row < 9; ++row) {
        for (int col = 0; col < 9; ++col) {
            cells[row][col] = other.cells[row][col];

            for (int k = 0; k < 9; ++k) {
                candidates[row][col][k] = other.candidates[row][col][k];
            }
        }
    }
}

int Field::putDigit(int row, int col, int digit) {
    if (row   < 0 || row   > 8 ||
        col   < 0 || col   > 8 ||
        digit < 0 || digit > 9) {
        return -1;
    }

    if (cells[row][col] != 0) {
        return -1;
    }

    cells[row][col] = digit;

    return 0;
}

int Field::putDigitUpdate(int row, int col, int digit) {
    if (digit == 0) {
        if (cells[row][col] == 0) {
            return 0;
        }
        else {
            return -1;
        }
    }

    if (putDigit(row, col, digit)) {
        return -1;
    }

    int r = (row / 3) * 3;
    int c = (col / 3) * 3;

    for (int i = 0; i < 9; ++i) {
        candidates[row][i][digit-1] = false;
        candidates[i][col][digit-1] = false;
        candidates[r+(i/3)][c+(i%3)][digit-1] = false;
    }

    return 0;
}

int Field::fileInput(FILE* input) {
    char t;
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            do {
                fscanf(input, "%c", &t);
            } while (!isdigit(t));

            if(putDigitUpdate(i, j, t - '0')) {
                std::cout << "Error!\n";
                return -1;
            }
        }
    }
    return 0;
}

int Field::fileOutput(FILE* output) {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            fprintf(output, "%d", cells[i][j]);
        }
        fprintf(output, "\n");
    }

    return 0;
}

int Field::fullCandidateUpdateSimple() {
    for (int row = 0; row < 9; ++row) {
        for (int col = 0; col < 9; ++col) {
            if (cells[row][col] == 0) {
                continue;
            }

            int d = cells[row][col];

            int r = (row / 3) * 3;
            int c = (col / 3) * 3;

            for (int i = 0; i < 9; ++i) {
                candidates[i][col][d-1] = false;
                candidates[row][i][d-1] = false;
                candidates[r+(i/3)][c+(i%3)][d-1] = false;
            }
        }
    }

    return 0;
}

bool Field::full() {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (cells[i][j] == 0) {
                return false;
            }
        }
    }

    return true;
}

int Field::openSingles() {
    int changed = 0;
    for (int row = 0; row < 9; ++row) {
        for (int col = 0; col < 9; ++col) {
            if (cells[row][col] != 0) {
                continue;
            }

            int t = 0;
            int f = 0;
            for (int k = 0; k < 9; ++k) {
                if (candidates[row][col][k]) {
                    ++f;
                    t = k + 1;
                }
            }

            if (f == 0) {
                return -1;
            }
            else if (f == 1) {
                putDigitUpdate(row, col, t);
                ++changed;
            }
        }
    }

    return changed;
}

int Field::closedSingles() {
    int changed = 0;

    for (int digit = 0; digit < 9; ++digit) {
        for (int b = 0; b < 9; ++b) {
            //---ROW
            int pos = -1;
            int wasOne = 0;
            bool already = false;

            for (int i = 0; i < 9; ++i) {
                if (cells[b][i] != 0) {
                    if (cells[b][i] == digit + 1) {
                        already = true;
                        break;
                    }
                    continue;
                }

                if (candidates[b][i][digit]) {
                    ++wasOne;
                    pos = i;
                }
            }

            if (!already) {
                if (wasOne == 0) {
                    return -1;
                }
                else if (wasOne == 1) {
                    putDigitUpdate(b, pos, digit + 1);
                    ++changed;
                }
            }

            //--COL

            pos = -1;
            wasOne = 0;
            already = false;

            for (int i = 0; i < 9; ++i) {
                if (cells[i][b] != 0) {
                    if (cells[i][b] == digit + 1) {
                        already = true;
                        break;
                    }
                    continue;
                }

                if (candidates[i][b][digit]) {
                    ++wasOne;
                    pos = i;
                }
            }

            if (!already) {
                if (wasOne == 0) {
                    return -1;
                }
                else if (wasOne == 1) {
                    putDigitUpdate(pos, b, digit + 1);
                    ++changed;
                }
            }

            //--BLOCK

            pos = -1;
            wasOne = 0;
            already = false;

            int fr = 3 * (b / 3);
            int fc = 3 * (b % 3);

            for (int i = 0; i < 9; ++i) {
                if (cells[fr+i%3][fc+i/3] != 0) {
                    if (cells[fr+i%3][fc+i/3] == digit + 1) {
                        already = true;
                        break;
                    }
                    continue;
                }

                if (candidates[fr+i%3][fc+i/3][digit]) {
                    ++wasOne;
                    pos = i;
                }
            }

            if (!already) {
                if (wasOne == 0) {
                    return -1;
                }
                else if (wasOne == 1) {
                    putDigitUpdate(fr + (pos % 3), fc + (pos / 3), digit + 1);
                    ++changed;
                }
            }
        }
    }

    return changed;
}

int Field::beginnerTry() {
    int status = 0;
    bool resume = true;

    while (resume) {
        resume = false;

        status = openSingles();

        if (status < 0) {
            return -1;
        }
        else if (status > 0) {
            resume = true;
        }

        status = closedSingles();
        if (status < 0) {
            return -1;
        }
        else if (status > 0) {
            resume = true;
        }
    }

    if (full()) {
        return 0;
    }
    else {
        return 1;
    }
}

int Field::recursiveSolve(int maxDepth) {
    //cout << "<" << maxDepth << ">\n";
    int status = beginnerTry();

    if (status == -1) {
        return status;
    }
    else if (status == 0) {
        return 0;
    }

    if (maxDepth < 1) {
        return 1;
    }

    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (cells[i][j] != 0) {
                continue;
            }

            for (int k = 0; k < 9; ++k) {
                if (candidates[i][j][k]) {
                    Field next = *this;

                    next.putDigitUpdate(i, j, k + 1);

                    status = next.recursiveSolve(maxDepth - 1);

                    if (status == 0) {
                        *this = next;
                        //copyField(next);
                        return 0;
                    }
                    else if (status == 1) {
                        ;
                    }
                    else if (status == -1) {
                        candidates[i][j][k] = false;
                    }
                }
            }
        }
    }

    return 1;
}

int Field::minDepth(int maxDepth) {
    int status;
    Field current = *this;
    for (int i = 0; i <= maxDepth; ++i) {
        Field temp = current;
        status = temp.recursiveSolve(i);
        if (status == 0) {
            *this = temp;
            return i;
        } else if (status == 1) {
            current = temp;
        } else if (status == -1) {
            return -2;
        }
    }

    return -1;
}

void Field::addTable(Table* table) {
    tables.push_back(table);
}
